#!/bin/sh

#check arch armv7l aarch64 mips 7401c0 sh4
arch=$(uname -m)
if [ "$arch" == "armv7l" ]; then
echo "> your device arch : $arch "

# Configuration
pack="ffmpeg"
version="6.1.1-r3"
url="https://gitlab.com/eliesat/dependencies/-/raw/main/"
package="ffmpeg"
temp_dir="/tmp"

# Determine package manager
if command -v dpkg &> /dev/null; then
    package_manager="apt"
    status_file="/var/lib/dpkg/status"
    install_command="dpkg -i --force-overwrite"
    uninstall_command="apt-get purge --auto-remove -y"
    plugin="$pack-$version.deb"
    echo "> your device is not supported" 
    sleep 3
    exit 1
else
    package_manager="opkg"
    status_file="/var/lib/opkg/status"
    install_command="opkg install --force-reinstall"
    uninstall_command="opkg remove --force-depends"
    plugin="$pack-$version.ipk"
fi

# Functions
print_message() {
    echo "[$(date +'%Y-%m-%d %H:%M:%S')] $1"
}

cleanup() {
    [ -d "/CONTROL" ] && rm -rf /CONTROL >/dev/null 2>&1
    rm -rf /control /postinst /preinst /prerm /postrm /tmp/*.ipk /tmp/*.tar.gz >/dev/null 2>&1
    print_message "Cleanup completed."
}

check_and_install_package() {
    if grep -q "$package" "$status_file"; then
        print_message "Removing existing $package package, please wait..."
        $uninstall_command $package
    fi

    print_message "Downloading $pack-$version, please wait..."
    wget -q --show-progress "$url/$plugin" -P "$temp_dir"
    if [ $? -ne 0 ]; then
        print_message "Failed to download $pack-$version from $url"
        exit 1
    fi

    print_message "Installing $pack-$version, please wait..."
    $install_command "$temp_dir/$plugin"
    if [ $? -eq 0 ]; then
        print_message "$pack-$version installed successfully."
    else
        print_message "Installation failed."
        exit 1
    fi
}

# Main
trap cleanup EXIT
check_and_install_package


else
echo "> your device arch : $arch is not supported"
sleep 3
exit 1
fi

